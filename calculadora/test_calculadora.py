from .calculadora import add, div, pot


def test_add():
    assert add(2, 3) == 5


def test_div():
    assert div(4, 2) == 2


def test_division_by_zero():
    assert div(7, 0) == 0


def test_potentiation_as_square_root():
    assert pot(4, (1 / 2)) == 2
